import { Component, OnInit } from '@angular/core';

import * as _ from 'lodash';
import { DateTime } from 'luxon';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { NzMenuThemeType } from 'ng-zorro-antd/menu';
import { VariableShareService } from '../../core/services/variable-share.service';

import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  isCollapsed: boolean = false;
  theme:NzMenuThemeType = 'light';
  private subscription?: Subscription;

  tabIndex = 0;
  companyId: any = '';

  loading = false;

  // maindd
  datasets: any = [];
  query: any = '';
  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;

  service_date: any = DateTime.now().toJSDate();

  constructor (
    private message: NzMessageService,
    private router: Router,
    private variableShareService: VariableShareService

  ) { 
    console.log('dashborad');
    this.subscription = this.variableShareService.sharedData$.subscribe(value => {
      this.isCollapsed = value;
     
    });

    this.subscription = this.variableShareService. sharedDataTheme$.subscribe(value => {     
      this.theme = value;
    });
  }
  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }
  closeSidebar(): void {
    this.isCollapsed = !this.isCollapsed;
  }
  executeParentMethod(): void {
    // This method will be triggered when the child emits the event
    this.closeSidebar();
    console.log('Parent method executed.');
  }

  getCloseSidebar(): void {
    this.isCollapsed = this.variableShareService.getCloseSidebar();
    console.log(this.isCollapsed);
  }

  getTheme(): void {
    this.theme = this.variableShareService.getTheme();
    console.log(this.theme);
  }

  ngOnInit() { }

  onBack(): void { }

  onTabChange(event: any) {
    this.tabIndex = event;
    if (event === 0) {
      // Tab 0
    } else if (event === 1) {
      // Tab 1
    } else {
      // Default tab
    }
  }

}
