import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SyncingChartComponent } from './syncing-chart.component';

describe('SyncingChartComponent', () => {
  let component: SyncingChartComponent;
  let fixture: ComponentFixture<SyncingChartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SyncingChartComponent]
    });
    fixture = TestBed.createComponent(SyncingChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
